## Interface: 80000
## Title: |cff20ff20Bagnon Guild Bank|r
## Author: Tuller & Jaliborc (João Cardoso)
## Notes: Single window display for your guild bank
## RequiredDeps: Bagnon
## LoadOnDemand: 1
## Version: 8.0.1

main.lua
components\components.xml
